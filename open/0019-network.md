- Feature Name: Network
- Start Date: 2019-12-16
- Tracking Issue: https://gitlab.com/veloren/rfcs/issues/17

# Summary

[summary]: #summary

This RFC is describing the Networking of veloren in an order to abstract away over multiple protocols to be a helpfull framework for the game.
The Motivation expains our goals, followed by a in detail terminology. After that we see an example how a game state could look like from Network perspective.

# Motivation

[motivation]: #motivation

We need to make sure we do networking right.
Currently it's a huge performance factor, our Ping are super high even for singleplayer.
For this networking solution (and not necessarily for all of voxygen and server) We want to:
 - have a system that hides server and client from a communication perspective, and just have 2 participants.
 - abstract multiple Protocols, like Tcp, Udp, and automatically use the best and whats available.
 - have a message based protocol even over Tcp which is (byte)stream based
 - allow for multiple streams of messages with different priorities (this streams does not refer to bytestreams like tcp, but more like streams in the http/2 api)
 - be copy free in order to be really efficient
 - don't lock threads longer then necessary
 - need not more threads than necessary and work on a threadpool
 - version the networking protocol and detect invalid combinations
 
Inpiratated by:
 - https://developers.google.com/web/fundamentals/performance/http2

### Participants
Participants marks all programs that want to communicate, E.g. 1 Server Participant, 3 Client participants.
Participants don't separate between Clients and Servers!

### Addresses
An Address contains all the information to open a channel from any Participant to the Participant of the address.
An example would be: TCP + IP address + Port

### Connection
a Connection is a abstract construct that exists when at least one Channel is available between 2 Participants.
The API exports the Connections and all operations are based on the Connection rather than on Channels directly.
The correct channel will be choosen by the Framework.

### Channel
A way to transmit data over any protocol, e.g. in order to create a TCP channel, Participants must start a TCP Client and TCP Server.
Currently we support TCP and UDP as Channel protocols => max 2 Channels per Connection

### Promises
Gurantees about messages, like messages are in order, or guaranted delivery or are verfified.

### Stream
Our network supports multiple Promises and Priorities, to make use of that, you have to open at least one Stream on a Connection.
When sending over the Stream, the Stream then will choose the right Channel to transport it's data.
When either participant opens a stream, the other participant will receive a notification and also open a Stream.

### Message
All payload data is send via messages. Messages must be (de)serializable and are send over one or multiple streams.
Sending a message over multiple streams, can be used as broadcast, it only requieres a single serialization, the message isn't split over multple streams.
However a message can be split up into multiple frames.
The API should feature a way to send broadcast messages

# Todo: Explain how Messages are Split up and Send here!

# Example

| PNo  | Participant   |
| ---: |:-------------:|
|  0   | Server        |
|  1   | Client Alice  |
|  2   | Client Bob    |
|  3   | Client Chad   |
|  4   | Server2       |
|  5   | Auth Server   |
|  6   | Client Dude   |
Note:
 - 4 Clients
 - 2 Servers
 - 1 Auth Server

| AddrNo | PNo  | Proto | Address                  |
| -----: | ---: |:-----:|:------------------------:|
|  0     |  0   | TCP   | server.veloren.net:40004 |
|  1     |  0   | UDP   | server.veloren.net:40004 |
|  2     |  2   | UDP   | 49.10.0.3:56012          |
|  3     |  3   | TCP   | 37.3.99.6:49011          |
|  4     |  3   | UDP   | 37.3.99.6:49011          |
|  5     |  4   | TCP   | 123.222.123.4:4411       |
|  6     |  4   | UDP   | 123.222.123.4:4412       |
|  7     |  5   | TCP   | auth.veloren.net:40006   |
Note:
 - PNo 1 is behind NAT and has no possible to access
 - PNo 2 can only access UDP
 - PNo 3 has full access
 - We need a way to get our own external IP address automatically, maybe request it from Auth server, and auth server must config it.

| ConnNo | PNo1  | PNo2 |
| -----: | ----: | ----:|
|  0     |  0    |  1   |
|  1     |  0    |  2   |
|  2     |  0    |  3   |
|  3     |  0    |  4   |
|  4     |  0    |  5   |
|  5     |  0    |  6   |
|  6     |  4    |  5   |
|  7     |  4    |  6   |
Note:
 - PNo1 and PNo2 in no special order

| ChNo | PFromNo  | AddrNo |
| ---: | -------: |:------:|
|  0   |  0       |  7     |
|  1   |  4       |  7     |
|  2   |  1       |  7     |
|  3   |  2       |  7     |
|  4   |  3       |  7     |
|  5   |  6       |  7     |
|  6   |  1       |  0     |
|  7   |  2       |  1     |
|  8   |  3       |  0     |
|  9   |  3       |  1     |
| 10   |  6       |  0     |
| 11   |  6       |  5     |
Note:
 - All connected to Auth
 - Clients connected to Server
 - Dude connected to Server2
 - TCP and UDP according to Addresses

| Promises   |
| :--------: |
| In Order   |
| No Corrupt |
| Guearanteed Delivery |
| Encrypted  |
Note:
 - No Corrput means i guearantee that the message either doesnt arrive or arrives Correctly

| Protocols   |
| :---------: |
| TCP         |
| UDP         |

| In Order | No Corrupt | Encrypted | Guearanteed | TCP  | UDP  |
| :------: | :--------: | :-------: | :---------: | ---: | ---: |
|    F     |    F       |    F      |    F        |   0  | 100  |
|    F     |    F       |    F      |    T        |   0  | 100  |
|    F     |    F       |    T      |    F        | 100  |  0   |
|    F     |    F       |    T      |    T        | 100  |  0   |
|    F     |    T       |    F      |    F        | 100  |  0   |
|    F     |    T       |    F      |    T        | 100  |  0   |
|    F     |    T       |    T      |    F        | 100  |  0   |
|    F     |    T       |    T      |    T        | 100  |  0   |
|    T     |    F       |    F      |    F        | 100  |  0   |
|    T     |    F       |    F      |    T        | 100  |  0   |
|    T     |    F       |    T      |    F        | 100  |  0   |
|    T     |    F       |    T      |    T        | 100  |  0   |
|    T     |    T       |    F      |    F        | 100  |  0   |
|    T     |    T       |    F      |    T        | 100  |  0   |
|    T     |    T       |    T      |    F        | 100  |  0   |
|    T     |    T       |    T      |    T        | 100  |  0   |
Note:
 - Promises / Protocol Matrix, gives a score to each combination from Promisses to the used Protocol.
   Will be needed to evaluate the used Protocol(=Channel) of a Stream

| SNo  | ConnNo   | Promises |Prio |
| ---: | -------: | --------:| ---:|
|  0   |  4       |  I N E G |  8  |
|  1   |  6       |  I N E G |  8  |
|  2   |  0       |  - N - - | 20  |
|  3   |  1       |  - N - - | 20  |
|  4   |  2       |  - N - - | 20  |
|  5   |  3       |  - N - - | 20  |
|  6   |  5       |  - N - - | 20  |
|  7   |  7       |  - N - - | 20  |
|  8   |  0       |  - N - G | 22  |
|  9   |  1       |  - N - G | 22  |
| 10   |  2       |  - N - G | 22  |
| 11   |  3       |  - N - G | 22  |
| 12   |  5       |  - N - G | 22  |
| 13   |  7       |  - N - G | 22  |
| 14   |  1       |  I N - G | 19  |
| 15   |  2       |  I N - G | 19  |
| 16   |  3       |  I N - G | 19  |
| 17   |  5       |  I N - G | 19  |
| 18   |  7       |  I N - G | 19  |
Note:
 - 1 Encrypted Stream to Auth servers (maybe we decide on that clients need to connect to AUTH direct,y, then ofc more)
 - 1 in Order Stream for messages
 - Terrain updates can be streamed about No Corrupt Streams with Prop 22 and Guraanteed Delivery
 - Entity Updates where order or Guraantee doesnt matter can be send with higer Prio
 - Very Prio In Order Events with even higher prio
 - The rule is! For every 2 Bytes of Prio N i Send 1 Byte of Prio N+1.
   For every Byte i Send with prio19 i try to send 2048 prio8 bytes.
   For every package of Terrain Data, i aprox send 8 Packages of more important Data
 - Ofc, if all higher Prio Streams cont contain Data, other Data is send directly!
 - Prios 0-7 are reserved for the networking routing internally


# Pseudo Coding

```rust
fn start_game() {
 let addr = (TCP, 'server.veloren.net:40004');
 let network = Network::new();
 let conn = network.open(&addr)?; //channels are managed by network based on requierements
 let joinstream = network.stream(&conn, [InOrder, NoCorrput, GuearanteedDelivery], 7)

 let joinmsg = IWantJoin {
    name: "Dude",
 }
 network.msg(conn, IWantJoin)
}
```

# open questions

## Plans for p2p
p2p is a difficult question, because it comes with a lot of risks and is complicated.
p2p means, that any client in a network can connect to any other client directly.
As this network RFC doesn't gives a strict client-server model but just a model how to connect 2 participants, so p2p isn't ruled out per se.

## Nat
some clients lay behind NATs and it's not possible to connect to their Address from the internet.
We could avoid that problem by supporting NATed connections.
This would especially be handy if we enable(requiere) p2p, because without p2p it's quite resonable to say as a server-owner you have to be accessible from the internet (=> port forwarding).
However we DO NOT want every client to manage port-forwarding in order to join a game.
We could implement this on top of this protocol by creating another PROTOCOL where the implementation sends that data via UDP or TCP and does some routing.
So NATing also isn't ruled out by this RFC, but it's not included by default.
currently we hope that addresses are accessable from everywhere, however we might need to add a flag "LAN" or "INTERNET" to it.

## Additional protocols
- MPSC: when using singleplayer we could just transfer the data via mpsc and even skip serialization. Or use pipe on a linux system
- QUIC to deliver messages

# Todo:
- Figure Out how to handle a full queue, if the queue is full we probably need to reduce data or our game will be lagging behind, we would need some minimal transmission speed which is necessary and the rest needs to be optional, e.g. a way for the server to detect slow internet and dont send 144 ticks/s but only 30. 
- research how UDP works exactly it's statless so there is no client and server, how to send messages back, i expect that SOME routers have SOME logic to detect that behind a NAT, how reliable is that?
- We want to verify our speed with a benchmark, create a benchmark and set some realistic threshhold. E.g. beeing able to Pipe around 10GBit of data without bottlenecks would be perfect because 10Gbit is what affortable professional servers have
- Research how exactly we implement Encryption

# Drawbacks

[drawbacks]: #drawbacks

- Complexity
- Hard to debug
- Performance Hits (e.g. for copying, or moving stream data into messages)

# Rationale and alternatives

[alternatives]: #alternatives


# Unresolved questions

[unresolved]: #unresolved-questions
